<?php include 'dbconnection.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>manage report</title>
    <link rel="stylesheet" href="../assets/navabr.css">
    <style>
        .bd-placeholder-img {
          font-size: 1.125rem;
          text-anchor: middle;
          -webkit-user-select: none;
          -moz-user-select: none;
          user-select: none;
        }
    
        @media (min-width: 768px) {
          .bd-placeholder-img-lg {
            font-size: 3.5rem;
          }
        }
      </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <header class="py-3 mb-4 border-bottom shadow">
        <div class="container-fluid align-items-center d-flex">
            <div class="flex-shrink-1">
                <a href="#" class="d-flex align-items-center col-lg-4 mb-2 mb-lg-0 link-dark text-decoration-none">
                    <!-- <i class="bi bi-bootstrap fs-2 text-dark"></i> -->
                </a>
            </div>
            <div class="flex-grow-1 d-flex align-items-center">
                <div class="w-100 me-3">
                    <h1>Bāngzhù tái    </h1>
                </div>
                
                <div class="flex-shrink-0 dropdown">
                    <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="https://via.placeholder.com/28?text=!" alt="user" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end shadow" aria-labelledby="dropdownUser2" >
                        <li><a class="dropdown-item" href="profile.php">Profile</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Sign out</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid pb-3 flex-grow-1 d-flex flex-column flex-sm-row overflow-auto">
        <div class="row flex-grow-sm-1 flex-grow-0">
            <aside class="col-sm-1 flex-grow-sm-1 flex-shrink-1 flex-grow-0 sticky-top pb-sm-0 ">
                <div class="bg-light border rounded-3  h-100 sticky-top">
                    <ul class="nav nav-pills flex-sm-column flex-row mb-auto justify-content-between text-truncate">
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 text-truncate">
                                <!-- <i class="bi bi-house fs-5"></i> -->
                                <img src="assets/img/dashboard.png" alt="">
                                <span class="d-none d-sm-inline"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-2 text-truncate">
                                <i class="bi bi-speedometer fs-5"></i>
                                <span class="d-none d-sm-inline"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-2 text-truncate"><i class="bi bi-card-text fs-5"></i>
                                <span class="d-none d-sm-inline"></span> </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-2 text-truncate"><i class="bi bi-bricks fs-10"></i>
                                <span class="d-none d-sm-inline"></span> </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-2 text-truncate"><i class="bi bi-people fs-5"></i>
                                <span class="d-none d-sm-inline"></span> </a>
                        </li>
                    </ul>
                </div>
            </aside>
            <main class="col-2 overflow-auto h-100">
            
      <div class="container">
        <div class="container-fluid">
          <p>All Tickets</p>
          <hr>
        <button type="button" class="btn  mb-4">
        Unassigned tickets
        <span class="badge badge-danger ms-2 btn-primary">8</span>
</button>
<button type="button" class="btn  mb-4">
Open Tickets<span class="badge badge-danger ms-2 btn-primary">8</span>
</button>
<hr>
<button type="button" class="btn  mb-4">
Open<span class="badge badge-danger ms-2 btn-primary">8</span>
</button>
<button type="button" class="btn  mb-4">
On going<span class="badge badge-danger ms-2 btn-primary">8</span>
</button>
<button type="button" class="btn  mb-4">
On hold<span class="badge badge-danger ms-2 btn-primary">8</span>
</button>
<button type="button" class="btn  mb-4">
Closed<span class="badge badge-danger ms-2 btn-primary">8</span>
</button>

        
        </div>
       
      
            </main>
            <main class="col-9 overflow-auto h-100">
            
            <div class="container">
              <div class="container-fluid">
              <div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card h-100">
     
      <div class="card-body">
        <h5 class="card-title">Total Tickets</h5>
       
        <span class="badge badge-danger ms-2 btn-primary"> 
          <?php
      $query = "SELECT * FROM generate_ticket ORDER BY ticket_id";  
      $query_run = mysqli_query($con, $query);
      $row = mysqli_num_rows($query_run);
      echo $row ;
?></span>
        
      </div>
      
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
     
      <div class="card-body">
        <h5 class="card-title">Total Closed</h5>
        <span class="badge badge-danger ms-2 btn-primary">
        <?php
      $query = "SELECT * FROM generate_ticket where ticket_status=1";  
      $query_run = mysqli_query($con, $query);
      $row = mysqli_num_rows($query_run);
      echo $row ;
?>
        
        </span>
        
      </div>
     
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
     
      <div class="card-body">
        <h5 class="card-title">Total Pending</h5>
        <span class="badge badge-danger ms-2 btn-primary">
        <?php
      $query = "SELECT * FROM generate_ticket where ticket_status=2";  
      $query_run = mysqli_query($con, $query);
      $row = mysqli_num_rows($query_run);
      echo $row ;
?>
        </span>
        
        
      </div>
      
    </div>
  </div>
</div>
<div class="col">
    <div class="card h-100">
     
      <div class="card-body">
        <h5 class="card-title">Total open</h5>
        <span class="badge badge-danger ms-2 btn-primary">
        <?php
      $query = "SELECT * FROM generate_ticket where ticket_status=4";  
      $query_run = mysqli_query($con, $query);
      $row = mysqli_num_rows($query_run);
      echo $row ;
?>
        </span>
        
        
      </div>
      
    </div>
  </div>
</div>


              
              </div>
            
                  </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" ></script>
  <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <!-- <script>
  $(document).ready( function () {
		$('.table').DataTable();
  });
  </script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->
</body>
</html>