<?php
include 'dbconnection.php';
$res=mysqli_query($con,"select * from generate_ticket");
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dynamic Datatable</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
	<div class="container" style="margin-top:50px;">
	  <table class="table table-striped">
		<thead>
			<tr>
				<th>Ticket_id</th>
				<th>FisrtName</th>
				<th>LastName</th>
				<th>email</th>
				<th>School</th>
				<th>Role</th>
				<th>TicketSubject</th>
				<th>Description</th>
				<th>TicketAttachment</th>
				<th>TicketStatus</th>
				<th>TicketImportance</th>
				<th>TicketAssign</th>
				<th>datetime</th>
			</tr>
		</thead>
		<tbody>
			<?php while($row=mysqli_fetch_assoc($res)){?>
			<tr>
				<td><?php echo $row['ticket_id']?></td>
				<td><?php echo $row['firstname']?></td>
				<td><?php echo $row['lastname']?></td>
				<td ><?php echo $row['email']?></td>
				<td><?php echo $row['school']?></td>
				<td><?php echo $row['role']?></td>
				<td><?php echo $row['ticketsubject']?></td>
				<td><?php echo $row['description']?></td>
				<td><?php echo $row['ticketattachment']?></td>
				<td><?php echo $row['ticket_status']?></td>
				<td><?php echo $row['ticket_importance']?></td>
				<td><?php echo $row['ticket_assign']?></td>
				<td><?php echo $row['datetime']?></td>
			</tr>
			<?php } ?>
		</thead>
	  </table>
   </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" ></script>
  <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready( function () {
		$('.table').DataTable();
  });
  </script>
</body>
</html>