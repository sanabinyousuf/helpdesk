<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<style>
   
    header nav a{
    color: white;
    margin-right: 50px;
    font-weight: 500;
    
    
}
header{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content:space-between;
    background-color: #589fe6;
    color: white;
    padding: 15px;
}

</style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Team Login</title>
  </head>
  <body>
  <!-- <div class="card text-white  bg-primary mb-3" style="">
   <div class="card-body">
    <h5 class="card-title text-center">TEAM LOGIN</h5>
  </div>
</div> -->
<div class="container">
    <form>
        
    <div class="shadow-lg p-5 mb-2 mt-5 bg-white rounded">
    <div class="card text-white bg-primary mt-2 mb-3" style="">
    <h5 class="card-title text-center">TEAM LOGIN</h5>
</div>
        
          <div class="form-group">
          
          <label for="teamlogin">UserName</label>
          <input type="username" class="form-control" aria-describedby="emailHelp" placeholder="Enter your username"required>
          <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
          <label for="teamlogin">Password</label>
          <input type="password" class="form-control" id="" placeholder="Password"required>
        </div>
        
        <div class="d-grid gap-2 d-md-flex justify-content-md-center">
  
  <button class="btn btn-success" type="button">Button</button>
</div>
        
      </form>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>