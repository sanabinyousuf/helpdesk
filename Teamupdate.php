<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<style>
    
    /* img {
        width:30px;
          height:10px;
      } */

header nav a{
    color: white;
    margin-right: 50px;
    font-weight: 500;
    
}
header{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content:space-between;
    background-color: #589fe6;
    color: white;
    padding: 15px;
}
/* img {
    width:100%;
    height: 230px;
} */

</style>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>update team</title>
    
  </head>
  <body>
  <div class="container">
  
  <!-- <div class="card text-white bg-primary mb-3" style="">
   <div class="card-body">
    <h5 class="card-title text-center" text="center">UPDATE TEAM DETAILS</h5>
  </div>
</div> -->
    <form>
        <div class="shadow-lg p-5 mb-2 mt-5 bg-white rounded">
        <div class="card text-white bg-primary mt-2 mb-3" style="">
   
    <h5 class="card-title text-center" text="center">UPDATE TEAM DETAILS</h5>
  </div>

       
        <div class="card-body">
                <div class="d-flex align-items-right">
                  <div>
                    <div class="text-left">
                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_1x.png" class="avatar img-circle img-fluid" alt="avatar" max-width=50% height=auto>
                        <!-- <h6>Upload a different photo...</h6> -->
                        <input type="file" class="text-center mb-2 mt-2 center-block file-upload">
                    </div>

                </div>
          </div>
        
        <div class="form-group">
            <label for="teamlogin">Update UserName</label>
            <input type="username" class="form-control" id="" placeholder="New UserName"required>
        </div>
        <div class="form-group">
            <label for="teamlogin">Update Password</label>
            <input type="password" class="form-control" id="" placeholder="New password"required>
        </div>

        <button type="button" class="btn btn-success">Update</button>
        </div>
      </form>  
        
          
       
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </div>


  <script>
     $(document).ready(function() {
      var readURL = function(input) 
      {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
      }
      $(".file-upload").on('change', function(){
    readURL(this);

  });
});
</script>
</body>
</html>